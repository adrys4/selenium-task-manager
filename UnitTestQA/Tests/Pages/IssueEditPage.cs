﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestSeleniumTaskManager.Services;

namespace TestSeleniumTaskManager.Tests.Pages
{
    public class IssueEditPage
    {
        [FindsBy(How = How.Id, Using = "SaveIssue")]
        public IWebElement SaveButton { get; set; }
        [FindsBy(How = How.Id, Using = "Title")]
        private IWebElement TitleField { get; set; }
        [FindsBy(How = How.Id, Using = "PriorityID")]
        private IWebElement PriorityList { get; set; }
        [FindsBy(How = How.Id, Using = "StatusID")]
        private IWebElement StatusList { get; set; }
        [FindsBy(How = How.Id, Using = "UserID")]
        private IWebElement DeveloperList { get; set; }
        [FindsBy(How = How.Id, Using = "Description")]
        private IWebElement DescriptionField { get; set; }

        WebDriver driver;

        public IssueEditPage (WebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public IssueEditPage EditIssue(string title,
                                    string priority,
                                    string status,
                                    string user,
                                    string description)
        {
            if (title != "") {
                TitleField.Clear();
                TitleField.SendKeys(title);
            }
            else {
                while (!driver.TextEqualByValue(TitleField,""))
                {
                    TitleField.Click();
                    TitleField.SendKeys(Keys.Backspace);
                    driver.ImplictWait(150);
                }
            }
            driver.SelectElementByText(PriorityList, priority);
            driver.SelectElementByText(StatusList, status);
            driver.SelectElementByText(DeveloperList, user);
            DescriptionField.Clear();
            DescriptionField.SendKeys(description);
            SaveButton.Click();
            return new IssueEditPage(driver);
        }
    }
}
