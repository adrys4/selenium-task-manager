﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestSeleniumTaskManager.Services;

namespace TestSeleniumTaskManager.Tests.Pages
{
    public class IssueIndexPage: AbstractPage
    {   
        [FindsBy(How = How.Id, Using = "Create")]
        public IWebElement CreateButton { get; set; }
        [FindsBy(How = How.CssSelector, Using = ".table")]
        public IWebElement Table { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(.,'Delete')]")]
        public IWebElement DeleteButtonIndex { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[contains(@value,'Delete')]")]
        private IWebElement DeleteButton { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@name='SearchTitle']")]
        private IWebElement SearchTitle { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@name='SearchPriority']")]
        private IWebElement SearchPriority { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@name='SearchStatus']")]
        private IWebElement SearchStatus { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@name='SearchDeveloper']")]
        private IWebElement SearchDeveloper { get; set; }
        [FindsBy(How = How.Id, Using = "Search")]
        private IWebElement Search { get; set; }
        [FindsBy(How = How.XPath, Using = "//a[contains(.,'Edit')]")]
        private IWebElement EditButtonIndex { get; set; }

        WebDriver driver;

        private const string Path = "/Issue";

        public IssueIndexPage(WebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }      

        public IssueIndexPage ClickOnCreateButton()
        {
            CreateButton.Click();
            return new IssueIndexPage(driver);
        }

        public IssueIndexPage ClickOnEditButton()
        {
            EditButtonIndex.Click();
            return new IssueIndexPage(driver);
        }

        public string GetUrlThisSite()
        {
            return Url + Path;
        }

        public IssueIndexPage DeleteAllIssues()
        {
            while (WebDriver.ElementExist(DeleteButtonIndex))
            {
                DeleteButtonIndex.Click();
                DeleteButton.Click();
            }
            return new IssueIndexPage(driver);
        }

        public IssueIndexPage UsingFilterBy(string filter, string value)
        {
            switch (filter)
            {
                case "title":
                    SearchTitle.SendKeys(value);
                    break;
                case "priority":
                    SearchPriority.SendKeys(value);
                    break;
                case "status":
                    SearchStatus.SendKeys(value);
                    break;
                case "developer":
                    SearchDeveloper.SendKeys(value);
                    break;
                default:
                    break;
            }
            Search.Click();
            return new IssueIndexPage(driver);
        }
    }
}
