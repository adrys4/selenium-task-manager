﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestSeleniumTaskManager.Services;

namespace TestSeleniumTaskManager.Tests.Pages
{
    public class LoginPage: AbstractPage
    {
        WebDriver driver;

        [FindsBy(How = How.Id, Using = "Login")]
        private IWebElement LoginField { get; set; }
        [FindsBy(How = How.Id, Using = "Password")]
        private IWebElement PasswordField { get; set; }
        [FindsBy(How = How.XPath, Using = "//input[@value='Log in']")]
        private IWebElement LoginButton { get; set; }
        [FindsBy(How = How.XPath, Using = "//div[@class='validation-summary-errors text-danger']")]
        public IWebElement Error { get; set; }

        private const string Path = "/Issue";

        public string INCORRECT_LOGIN = "Incorrect login attempt.";

        public LoginPage(WebDriver driver)
        {
            this.driver = driver;
            driver.Navigate().GoToUrl(Url + Path);
            PageFactory.InitElements(driver, this);
        }      

        public LoginPage LoginOnPage(string login, string password)
        {
            LoginField.SendKeys(login);
            PasswordField.SendKeys(password);
            LoginButton.Click();
            return this;
        }

        public LoginPage LoginOnPageContinious(string login, string password)
        {
            LoginField.SendKeys(login);
            PasswordField.SendKeys(password);
            LoginButton.Click();
            return new LoginPage(driver);
        }

        public string GetUrlExpectedSite()
        {
            return Url + Path;
        }
    }
}
