﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestSeleniumTaskManager.Services;

namespace TestSeleniumTaskManager.Tests.Pages
{
    public class IssueCreatePage: AbstractPage
    {
        [FindsBy(How = How.Id, Using = "Title")]
        private IWebElement TitleField { get; set; }
        [FindsBy(How = How.Id, Using = "PriorityID")]
        private IWebElement PriorityList { get; set; }
        [FindsBy(How = How.Id, Using = "StatusID")]
        private IWebElement StatusList { get; set; }
        [FindsBy(How = How.Id, Using = "UserID")]
        private IWebElement DeveloperList { get; set; }
        [FindsBy(How = How.Id, Using = "Description")]
        private IWebElement DescriptionField { get; set; }
        [FindsBy(How = How.Id, Using = "CreateIssue")]
        private IWebElement CreateButton { get; set; }

        private const string Path = "/Issue/Create";

        WebDriver driver;

        public IssueCreatePage(WebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public IssueCreatePage CreateIssue(string title, 
                                            string priority, 
                                            string status, 
                                            string user, 
                                            string description) {
            TitleField.SendKeys(title);
            driver.SelectElementByText(PriorityList, priority);
            driver.SelectElementByText(StatusList, status);
            driver.SelectElementByText(DeveloperList, user);
            DescriptionField.SendKeys(description);
            CreateButton.Click();
            return this;
        }

        public string GetUrlThisSite()
        {
            return Url + Path;
        }
    }
}
