﻿using NUnit.Framework;
using TestSeleniumTaskManager.Services;
using TestSeleniumTaskManager.Tests.Pages;
using TechTalk.SpecFlow;

namespace TestSeleniumTaskManager.Tests.Steps
{
    [Binding]
    public class LoginSteps
    {
        WebDriver driver;
        LoginPage LoginOnPage;

        [When(@"User log in with username (.*) and password (.*)")]
        public void UserLogIn(string Login, string Password)
        {
            driver = new WebDriver();
            LoginOnPage = new LoginPage(driver);
            LoginOnPage.LoginOnPage(Login, Password);
        }

        [Then(@"Logged Successfull")]
        public void LoggedSuccessfull()
        {
            Assert.AreEqual(driver.Url, LoginOnPage.GetUrlExpectedSite());
        }

        [Then(@"Logged Failed")]
        public void LoggedFailed()
        {
            Assert.AreNotEqual(driver.Url, LoginOnPage.GetUrlExpectedSite());
            Assert.IsTrue(driver.TextContains(LoginOnPage.Error, LoginOnPage.INCORRECT_LOGIN));
        }

        [AfterScenario]
        public void Close()
        {
            if (driver != null)
            {
                driver.Dispose();
                driver.Quit();
            }
        }
    }
}
