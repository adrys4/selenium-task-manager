﻿using NUnit.Framework;
using TestSeleniumTaskManager.Services;
using TestSeleniumTaskManager.Tests.Pages;
using TechTalk.SpecFlow;

namespace TestSeleniumTaskManager.Tests.Issue
{
    [Binding]
    public class IssueSteps 
    {
        WebDriver driver;
        LoginPage LoginOnPage;
        IssueIndexPage IssueIndex;
        IssueCreatePage IssueCreate;
        IssueEditPage IssueEdit;
        string[] TableElements;

        [Given(@"User Login with username (.*), password (.*)")]
        public void Login(string username, string password)
        {
            driver = new WebDriver();
            LoginOnPage = new LoginPage(driver);
            LoginOnPage = LoginOnPage.LoginOnPage(Accounts.LOGINADMIN, Accounts.PASSWORDADMIN);
        }

        [When(@"User Create Issue with title (.*), priority (.*), status (.*), user (.*), description (.*)")]
        public void UserCreateIssue(string title,
                                    string priority,
                                    string status,
                                    string user,
                                    string description) {
            IssueIndex = new IssueIndexPage(driver).ClickOnCreateButton();
            IssueCreate = new IssueCreatePage(driver).CreateIssue(
                                                                title,
                                                                priority,
                                                                status,
                                                                user,
                                                                description);
            TableElements = new string[] { title, priority, status, user };
        }

        [When(@"User delete all Issues")]
        public void WhenUserDeleteAllIssues()
        {
            IssueIndex = new IssueIndexPage(driver).DeleteAllIssues();
        }

        [When(@"User use Filter by (.*) with value (.*)")]
        public void WhenUserUseFilterWithValue(string filter, string value)
        {
            IssueIndex = new IssueIndexPage(driver).UsingFilterBy(filter,value);
        }

        [When(@"User Edit Issue with title (.*), priority (.*), status (.*), user (.*), description (.*)")]
        public void UserEditIssue(string title,
                                    string priority,
                                    string status,
                                    string user,
                                    string description) {
            IssueIndex = new IssueIndexPage(driver).ClickOnEditButton();
            IssueEdit = new IssueEditPage(driver).EditIssue(
                                                            title,
                                                            priority,
                                                            status,
                                                            user,
                                                            description);
            TableElements = new string[] { title, priority, status, user };
        }

        [Then(@"Table showed value (.*)")]
        public void ThenTableShowedElementsWithValue(string value)
        {
            Assert.IsTrue(driver.TextContains(IssueIndex.Table, value));
        }

        [Then(@"Table not showed value (.*)")]
        public void ThenTableNotShowedElementsWithValue(string value)
        {
            Assert.IsFalse(driver.TextContains(IssueIndex.Table, value));
        }

        [Then(@"Issue created successfull")]
        public void IssueCreatedSuccessfull()
        {
            Assert.AreEqual(driver.Url, IssueIndex.GetUrlThisSite());
            for (var i = 0; i < TableElements.Length; i++) {
                Assert.IsTrue(driver.TextContains(IssueIndex.Table, TableElements[i]));
            }
        }

        [Then(@"Issue edit successfull")]
        public void IssueEditSuccessfull()
        {
            Assert.AreEqual(driver.Url, IssueIndex.GetUrlThisSite());
            for (var i = 0; i < TableElements.Length; i++)
            {
                Assert.IsTrue(driver.TextContains(IssueIndex.Table, TableElements[i]));
            }
        }

        [Then(@"Issue edit failed")]
        public void IssueEditFailed()
        {
            Assert.AreNotEqual(driver.Url, IssueIndex.GetUrlThisSite());
            Assert.IsFalse(IssueEdit.SaveButton.Enabled);
        }

        [Then(@"Issue was not created")]
        public void IssueWasNotCreated()
        {
            Assert.AreNotEqual(driver.Url, IssueIndex.GetUrlThisSite());
            Assert.IsTrue(driver.TextContains(LoginOnPage.Error, "Incorrect login attempt."));
        }

        [Then(@"Issues was not displayed after delete")]
        public void ThenIssuesWasNotDisplayedAfterDelete()
        {
            Assert.IsFalse(WebDriver.ElementExist(IssueIndex.DeleteButtonIndex));
        }

        [AfterScenario]
        public void Close()
        {
            if (driver != null)
            {
                driver.Dispose();
                driver.Quit();
            }         
        }
    }
}
