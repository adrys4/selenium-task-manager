﻿Feature: IssueSteps

Scenario Outline: Successfull Create Issue
	Given User Login with username Admin, password Selenium879*
	When User Create Issue with title <title>, priority <priority>, status <status>, user <user>, description <description>
	Then Issue created successfull

	Examples: 
	| title		| priority  | status | user   | description    |
	| Zadanie 1 | Low		| Open	 | Admin  | Opis Zadania 1 |
	| Zadanie 2 | Medium	| Close  | adrys4 | Opis Zadania 2 |
	| Zadanie 3 | High		| Open   | Admin  | Opis Zadania 3 |

Scenario Outline: Successfull Use Filters
	Given User Login with username Admin, password Selenium879*
	When User use Filter by <filter> with value <value>
	Then Table showed value <value>

	Examples:
	| filter   | value     |
	| title    | Zadanie 1 |
	| priority | Low       |
	| status   | Close     |
	| user     | Admin     |

Scenario Outline: Failed Use Filters
	Given User Login with username Admin, password Selenium879*
	When User use Filter by <filter> with value <value>
	Then Table not showed value <value>

	Examples:
	| filter   | value			   |
	| title    | NotExistsTitle	   |
	| priority | NotExistsPriority |
	| status   | NotExistsStatus   |
	| user     | NotExistsUser     |

Scenario: Successfull Edit Issue
	Given User Login with username Admin, password Selenium879*
	When User Edit Issue with title CustomTitleXYZ, priority Medium, status Close, user adrys4, description CustomDescriptionXYZ
	Then Issue edit successfull

Scenario: Failed Edit Issue
	Given User Login with username Admin, password Selenium879*
	When User Edit Issue with title , priority Medium, status Close, user adrys4, description CustomDescriptionXYZ
	Then Issue edit failed

Scenario: Delete all Issues
	Given User Login with username Admin, password Selenium879*
	When User delete all Issues
	Then Issues was not displayed after delete