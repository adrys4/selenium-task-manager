﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace TestSeleniumTaskManager.Services
{
    public class WebDriver : IWebDriver
    {
        public readonly IWebDriver driver;

        public WebDriver()
        {          
            ChromeOptions chrome = new ChromeOptions();
            chrome.AddArgument("--start-maximized");
            chrome.AddArguments("headless");
            driver = new ChromeDriver(chrome);
            ConfigureTimeouts();
        }

        private void ConfigureTimeouts()
        {
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(10);
        }

        public void ExecuteJS(string script)
        {
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript(script);
        }

        public void Dispose()
        {
            this.driver.Dispose();
        }
        public void ImplictWait(int milis)
        {
            Thread.Sleep(milis);
        }
        public IWebElement FindElement(By by)
        {
            return this.driver.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return this.driver.FindElements(by);
        }

        public void Close()
        {
            this.driver.Close();
        }

        public void Quit()
        {
            this.driver.Quit();
        }

        public IOptions Manage()
        {
            return this.driver.Manage();
        }

        public INavigation Navigate()
        {
            return this.driver.Navigate();
        }

        public void Get(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public ITargetLocator SwitchTo()
        {
            return this.SwitchTo();
        }

        public string Url
        {
            get
            {
                return this.driver.Url;
            }
            set
            {
                this.driver.Url = value;
            }
        }

        public string Title
        {
            get
            {
                return this.driver.Title;
            }
        }

        public string PageSource
        {
            get
            {
                return this.driver.PageSource;
            }
        }

        public string CurrentWindowHandle
        {
            get
            {
                return this.driver.CurrentWindowHandle;
            }
        }

        public ReadOnlyCollection<string> WindowHandles
        {
            get
            {
                return this.WindowHandles;
            }
        }

        public bool TextContains(IWebElement element, string text)
        {
            if (element.Text.Contains(text))
            {
                return true;
            }
            return false;
        }

        public bool TextEqualByValue(IWebElement element, string text)
        {
            var val = element.GetAttribute("value");
            if (val.Equals(text))
            {
                return true;
            }
            return false;
        }
        public void SelectElementByText(IWebElement element, string text)
        {
            SelectElement ele = new SelectElement(element);
            if (!ele.SelectedOption.ToString().Contains(text)) {
                ele.SelectByText(text);
            }
        }

        public static bool ElementExist(IWebElement element)
        {
            try
            {
                if (!element.Size.IsEmpty)
                {
                    return true;
                }
                return false;
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }
    }
}
